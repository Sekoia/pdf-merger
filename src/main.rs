#![cfg_attr(
    all(
        target_os = "windows",
        not(debug_assertions),
    ),
    windows_subsystem = "windows"
)]
mod toast;

use std::collections::BTreeMap;
use std::path::PathBuf;
use iced::{Alignment, Application, Command, Element, Event, executor, font, Font, Length, Renderer, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced::Subscription;
use iced::widget::{button, column, Column, container, row, scrollable, text, text_input};
use iced::window;
use lopdf::{Bookmark, Document, Object, ObjectId};
use crate::toast::{Status, Toast};

pub fn main() -> iced::Result {
    PdfMerger::run(iced::Settings::default())
}

struct PdfInfo {
    path: PathBuf,
    pages: usize,
    pages_kept_pattern: String,
    document: Document,
}

struct PdfMerger {
    pdfs: Vec<PdfInfo>,
    toast: Option<Toast>,
    prev_path: Option<PathBuf>,
}

#[derive(Debug, Clone)]
enum Message {
    AddPdf,
    MovePdfUp(usize),
    MovePdfDown(usize),
    RemovePdf(usize),
    UpdatePatternString(usize, String),
    ExecuteMerge,
    CloseToast,

    Event(Event),
    FontLoaded(Result<(), font::Error>),
}

fn merge_documents(documents: Vec<Document>) -> Option<Document> {
    // Define a starting max_id (will be used as start index for object_ids)
    let mut max_id = 1;
    let mut pagenum = 1;
    // Collect all Documents Objects grouped by a map
    let mut documents_pages = BTreeMap::new();
    let mut documents_objects = BTreeMap::new();
    let mut document = Document::with_version("1.5");

    for mut doc in documents {
        let mut first = false;
        doc.renumber_objects_with(max_id);

        max_id = doc.max_id + 1;

        documents_pages.extend(
            doc
                .get_pages()
                .into_iter()
                .map(|(_, object_id)| {
                    if !first {
                        let bookmark = Bookmark::new(String::from(format!("Page_{}", pagenum)), [0.0, 0.0, 1.0], 0, object_id);
                        document.add_bookmark(bookmark, None);
                        first = true;
                        pagenum += 1;
                    }

                    (
                        object_id,
                        doc.get_object(object_id).unwrap().to_owned(),
                    )
                })
                .collect::<BTreeMap<ObjectId, Object>>(),
        );
        documents_objects.extend(doc.objects);
    }

    // Catalog and Pages are mandatory
    let mut catalog_object: Option<(ObjectId, Object)> = None;
    let mut pages_object: Option<(ObjectId, Object)> = None;

    // Process all objects except "Page" type
    for (object_id, object) in documents_objects.iter() {
        // We have to ignore "Page" (as are processed later), "Outlines" and "Outline" objects
        // All other objects should be collected and inserted into the main Document
        match object.type_name().unwrap_or("") {
            "Catalog" => {
                // Collect a first "Catalog" object and use it for the future "Pages"
                catalog_object = Some((
                    if let Some((id, _)) = catalog_object {
                        id
                    } else {
                        *object_id
                    },
                    object.clone(),
                ));
            }
            "Pages" => {
                // Collect and update a first "Pages" object and use it for the future "Catalog"
                // We have also to merge all dictionaries of the old and the new "Pages" object
                if let Ok(dictionary) = object.as_dict() {
                    let mut dictionary = dictionary.clone();
                    if let Some((_, ref object)) = pages_object {
                        if let Ok(old_dictionary) = object.as_dict() {
                            dictionary.extend(old_dictionary);
                        }
                    }

                    pages_object = Some((
                        if let Some((id, _)) = pages_object {
                            id
                        } else {
                            *object_id
                        },
                        Object::Dictionary(dictionary),
                    ));
                }
            }
            "Page" => {}     // Ignored, processed later and separately
            "Outlines" => {} // Ignored, not supported yet
            "Outline" => {}  // Ignored, not supported yet
            _ => {
                document.objects.insert(*object_id, object.clone());
            }
        }
    }

    // If no "Pages" object found abort
    if pages_object.is_none() {
        return None;
    }

    // Iterate over all "Page" objects and collect into the parent "Pages" created before
    for (object_id, object) in documents_pages.iter() {
        if let Ok(dictionary) = object.as_dict() {
            let mut dictionary = dictionary.clone();
            dictionary.set("Parent", pages_object.as_ref().unwrap().0);

            document
                .objects
                .insert(*object_id, Object::Dictionary(dictionary));
        }
    }

    // If no "Catalog" found abort
    if catalog_object.is_none() {
        return None;
    }

    let catalog_object = catalog_object.unwrap();
    let pages_object = pages_object.unwrap();

    // Build a new "Pages" with updated fields
    if let Ok(dictionary) = pages_object.1.as_dict() {
        let mut dictionary = dictionary.clone();

        // Set new pages count
        dictionary.set("Count", documents_pages.len() as u32);

        // Set new "Kids" list (collected from documents pages) for "Pages"
        dictionary.set(
            "Kids",
            documents_pages
                .into_iter()
                .map(|(object_id, _)| Object::Reference(object_id))
                .collect::<Vec<_>>(),
        );

        document
            .objects
            .insert(pages_object.0, Object::Dictionary(dictionary));
    }

    // Build a new "Catalog" with updated fields
    if let Ok(dictionary) = catalog_object.1.as_dict() {
        let mut dictionary = dictionary.clone();
        dictionary.set("Pages", pages_object.0);
        dictionary.remove(b"Outlines"); // Outlines not supported in merged PDFs

        document
            .objects
            .insert(catalog_object.0, Object::Dictionary(dictionary));
    }

    document.trailer.set("Root", catalog_object.0);

    // Update the max internal ID as wasn't updated before due to direct objects insertion
    document.max_id = document.objects.len() as u32;

    // Reorder all new Document objects
    document.renumber_objects();

    //Set any Bookmarks to the First child if they are not set to a page
    document.adjust_zero_pages();

    //Set all bookmarks to the PDF Object tree then set the Outlines to the Bookmark content map.
    if let Some(n) = document.build_outline() {
        if let Ok(x) = document.get_object_mut(catalog_object.0) {
            if let Object::Dictionary(ref mut dict) = x {
                dict.set("Outlines", Object::Reference(n));
            }
        }
    }

    document.compress();

    Some(document)
}

impl PdfMerger {
    fn execute_merge(&self) -> Result<Document, String> {
        fn parse_range(range: String) -> Result<Vec<(usize, usize)>, String> {
            range.split(',').map(|x| {
                if let Ok(x) = x.parse::<usize>() {
                    return Ok((x, x));
                }
                let (left, right) = x.split_once('-').ok_or_else(|| format!("Bad pattern: {}", x))?;

                let left = left.parse::<usize>().ok().ok_or_else(|| format!("Bad pattern: {}", x))?;
                let right = right.parse::<usize>().ok().ok_or_else(|| format!("Bad pattern: {}", x))?;
                Ok((left, right))
            }).collect()
        }

        let mut doc_list = Vec::with_capacity(self.pdfs.len());


        for pdf in &self.pdfs {
            let mut new_doc = pdf.document.clone();
            if !pdf.pages_kept_pattern.is_empty() {
                let ranges = parse_range(pdf.pages_kept_pattern.clone())?;

                'page: for i in (1..=pdf.pages).rev() {
                    for (start, end) in &ranges {
                        if *start <= i && i <= *end {
                            continue 'page;
                        }
                    }
                    new_doc.delete_pages(&[i as u32]);
                }
            }

            doc_list.push(new_doc);
        }

        merge_documents(doc_list).ok_or_else(|| "Merging Failed".to_string())
    }

    fn add_pdf(&mut self, path: PathBuf) {
        self.prev_path = Some(path.clone());
        let document = Document::load(&path);
        match document {
            Ok(document) => {
                self.pdfs.push(
                    PdfInfo {
                        path,
                        pages: document.get_pages().len(),
                        pages_kept_pattern: String::new(),
                        document,
                    }
                );
            }
            Err(e) => {
                self.toast = Some(
                    Toast {
                        title: "Error loading file".to_string(),
                        body: e.to_string(),
                        status: Status::Danger,
                    }
                )
            }
        }
    }
}

impl Application for PdfMerger {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            PdfMerger {
                pdfs: Vec::new(),
                toast: None,
                prev_path: directories::UserDirs::new().map(|x| x.home_dir().to_path_buf()),
            },
            font::load(include_bytes!("MaterialIcons-Regular.ttf").as_slice()).map(Message::FontLoaded),
        )
    }

    fn title(&self) -> String {
        String::from("PDF Merger")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::AddPdf => {
                let pdf = rfd::FileDialog::new()
                    .add_filter("PDF", &["pdf"])
                    .set_title("Select PDF");
                let pdf = if let Some(x) = &self.prev_path {
                    if x.is_file() {
                        pdf
                            .set_directory(x.parent().unwrap())
                            .set_file_name(x.file_name().unwrap().to_str().unwrap())
                    } else {
                        pdf.set_directory(x)
                    }
                } else {
                    pdf
                };
                let pdf = pdf.pick_file();
                if let Some(path) = pdf {
                    if !path.is_file() {
                        self.toast = Some(
                            Toast {
                                title: "Error adding file".to_string(),
                                body: "Not a file".to_string(),
                                status: Status::Danger,
                            }
                        );
                    } else {
                        self.add_pdf(path);
                    }
                }
            }
            Message::MovePdfUp(x) => {
                if x > 0 {
                    self.pdfs.swap(x, x - 1);
                }
            }
            Message::MovePdfDown(x) => {
                if x < self.pdfs.len() - 1 {
                    self.pdfs.swap(x, x + 1);
                }
            }
            Message::RemovePdf(x) => {
                self.pdfs.remove(x);
            }
            Message::ExecuteMerge => {
                if self.pdfs.len() == 0 {
                    self.toast = Some(
                        Toast {
                            title: "Error merging".to_string(),
                            body: "No PDFs to merge".to_string(),
                            status: Status::Danger,
                        }
                    );
                    return Command::none();
                }

                let path = &self.pdfs[0].path;

                let pdf = rfd::FileDialog::new()
                    .add_filter("PDF", &["pdf"])
                    .set_title("Save PDF")
                    .set_directory(path.parent().unwrap())
                    .set_file_name(path.file_name().unwrap().to_str().unwrap());

                let pdf = pdf.save_file();
                if let Some(pdf) = pdf {
                    if let Err(e) = self.execute_merge().and_then(|mut doc| doc.save(pdf).map_err(|e| e.to_string())) {
                        self.toast = Some(
                            Toast {
                                title: "Error merging".to_string(),
                                body: e,
                                status: Status::Danger,
                            }
                        );
                    } else {
                        self.toast = Some(
                            Toast {
                                title: "Success".to_string(),
                                body: "Successfully merged!".to_string(),
                                status: Status::Success,
                            }
                        )
                    }
                }
            }
            Message::UpdatePatternString(i, s) => {
                self.pdfs[i].pages_kept_pattern = s;
            }
            Message::CloseToast => {
                self.toast = None
            }
            Message::Event(e) => {
                if let Event::Window(e) = e {
                    match e {
                        window::Event::FileDropped(file) => {
                            if file.extension() == Some("pdf".as_ref()) {
                                self.add_pdf(file)
                            } else {
                                self.toast = Some(
                                    Toast {
                                        title: "Error loading file".to_string(),
                                        body: "Not a PDF file".to_string(),
                                        status: Status::Danger,
                                    }
                                );
                            }
                        }
                        _ => {}
                    }
                }
            }
            Message::FontLoaded(_) => {}
        }

        Command::none()
    }

    fn view(&self) -> Element<'_, Self::Message, Renderer<Self::Theme>> {
        const ICONS: Font = Font::with_name("Material Icons");
        let icon_button = |unicode: char| {
            button(
                text(unicode.to_string())
                    .font(ICONS)
                    .horizontal_alignment(Horizontal::Center)
                    .vertical_alignment(Vertical::Center),
            )
                .padding(10)
                .width(40)
        };

        let button = |label| {
            button(
                text(label).horizontal_alignment(Horizontal::Center),
            )
                .padding(10)
                .width(80)
        };

        let top_row = row![
            button("Add PDF").width(Length::Fill).on_press(Message::AddPdf),
            button("Merge").width(Length::Fill).on_press(Message::ExecuteMerge),
        ].align_items(Alignment::Center).spacing(20);

        let pdf_list: Vec<_> = self.pdfs.iter().enumerate().filter_map(|(i, x)| {
            if let Some(stem) = x.path.file_stem() {
                Some(
                    row![
                        text(format!("{}, {} pages", stem.to_str().unwrap(), x.pages)).vertical_alignment(Vertical::Center),
                        icon_button('\u{e5d8}').on_press(Message::MovePdfUp(i)),
                        icon_button('\u{e5db}').on_press(Message::MovePdfDown(i)),
                        button("Remove").on_press(Message::RemovePdf(i)),
                        text_input("Pages to keep (empty for all)", x.pages_kept_pattern.as_str()).on_input(move |x| Message::UpdatePatternString(i, x))
                    ].spacing(5).align_items(Alignment::Center).into()
                )
            } else {
                None
            }
        }).collect();

        let content = scrollable(Column::with_children(pdf_list)
            .align_items(Alignment::Center)
            .spacing(20));

        let content = container(column![top_row, content].spacing(20))
            .width(Length::Fill)
            .height(Length::Fill)
            .padding(20);

        toast::Manager::new(
            content,
            self.toast.as_ref()
                .map(core::slice::from_ref)
                .unwrap_or_default(),
            |_| Message::CloseToast,
        )
            .timeout(4)
            .into()
    }

    fn subscription(&self) -> Subscription<Message> {
        iced::subscription::events().map(Message::Event)
    }
}